package utilities.enumerations;

/**
 * Enumeration of possible holding status of a card.
 */
public enum HoldingStatus {
    /**
     * Player has the card.
     */
    HAS,

    /**
     * Player may have the card.
     */
    MAYBE,

    /**
     * Player doesn't have the card.
     */
    HASNOT
}