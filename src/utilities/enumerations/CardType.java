package utilities.enumerations;

/**
 * Enumeration for all the types of card.
 */
public enum CardType {

    /**
     * The card represents a character.
     */
    CHARACTER,

    /**
     * The card represents a weapon.
     */
    WEAPON,

    /**
     * The card represent a room.
     */
    ROOM;
}